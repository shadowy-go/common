# Shadow Go Common Library

[![pipeline status](https://gitlab.com/shadowy-go/common/badges/master/pipeline.svg)](https://gitlab.com/shadowy-go/common/commits/master)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/gate?key=common)](https://sonar.shadowy.eu/dashboard/index/common)

[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=common&metric=lines)](https://sonar.shadowy.eu/dashboard/index/common)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=common&metric=comment_lines_density)](https://sonar.shadowy.eu/dashboard/index/common)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=common&metric=function_complexity)](https://sonar.shadowy.eu/dashboard/index/common)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=common&metric=blocker_violations)](https://sonar.shadowy.eu/dashboard/index/common)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=common&metric=critical_violations)](https://sonar.shadowy.eu/dashboard/index/common)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=common&metric=code_smells)](https://sonar.shadowy.eu/dashboard/index/common)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=common&metric=bugs)](https://sonar.shadowy.eu/dashboard/index/common)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=common&metric=vulnerabilities)](https://sonar.shadowy.eu/dashboard/index/common)
[![Quality Gate](https://sonar.shadowy.eu/api/badges/measure?key=common&metric=sqale_debt_ratio)](https://sonar.shadowy.eu/dashboard/index/common)

Containts common function for
 * Array
 * Create simple service