package common

import (
	"flag"
	"os"
	"path"
	"path/filepath"
	"strings"
)

var applicationPath *string = nil
var root = flag.String("root", "", "path to application")

func ApplicationPath() string {
	if applicationPath == nil {
		if root != nil && *root != "" {
			applicationPath = root
		} else {
			var app string
			app, _ = os.Executable()
			app = path.Dir(strings.Replace(app, "\\", "/", -1))
			applicationPath = &app
		}
	}
	return *applicationPath
}

func GetAbsolutePath(p string) string {
	if filepath.IsAbs(p) {
		return p
	}
	return path.Join(ApplicationPath(), p)
}
