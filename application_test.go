package common

import (
	"testing"
)

func TestApplicationPathWithoutFlag(t *testing.T) {
	var path1 = ApplicationPath()
	if path1 == "" {
		t.Error("Application path should be not empty")
	}
	var path2 = ApplicationPath()
	if path1 != path2 {
		t.Error("Application path should be the same")
	}
}

func TestGetAbsolutePath(t *testing.T) {
	var path1 = GetAbsolutePath("./test")
	if path1 == "" {
		t.Error("Absoulte path should be not empty")
	}
	var path2 = GetAbsolutePath("./test")
	if path1 != path2 {
		t.Error("Absoulte path should be the same")
	}
	path1 = GetAbsolutePath("/test")
	if path1 != "/test" {
		t.Error("Absoulte \"/test\" path should be the same")
	}
}
