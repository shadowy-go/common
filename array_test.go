package common

import "testing"

func TestArrayInArrayIsExist(t *testing.T) {
	var array = []string{"a", "b", "c", "d"}
	exist, pos := InArray("a", array)
	if !exist {
		t.Error("Not exists")
	}
	if pos != 0 {
		t.Error("pos != 0")
	}
}

func TestArrayInArrayIsNotExist(t *testing.T) {
	var array = []string{"a", "b", "c", "d"}
	exist, pos := InArray("aa", array)
	if exist {
		t.Error("Exists")
	}
	if pos != -1 {
		t.Error("pos != -1")
	}
}

func TestConcatCopyPreAllocate(t *testing.T) {
	var test = [][]byte{[]byte("HELLO"), []byte(" "), []byte("WORLD")}
	var result = string(ConcatCopyPreAllocate(test))
	if result != "HELLO WORLD" {
		t.Error("result != 'HELLO WORLD'")
	}
}
