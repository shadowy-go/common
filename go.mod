module gitlab.com/shadowy-go/common

require (
	github.com/lestrrat-go/file-rotatelogs v0.0.0-20180607094457-00616292e771
	github.com/lestrrat-go/strftime v0.0.0-20180414112801-59966ecb6d84
	github.com/mattn/go-colorable v0.0.9
	github.com/mattn/go-isatty v0.0.3
	github.com/pkg/errors v0.8.0
	github.com/rifflock/lfshook v0.0.0-20180313183424-62f5e0662816
	github.com/sirupsen/logrus v1.0.5
	golang.org/x/crypto v0.0.0-20180608092829-8ac0e0d97ce4
	golang.org/x/sys v0.0.0-20180606202747-9527bec2660b
	gopkg.in/yaml.v2 v2.2.1
)
