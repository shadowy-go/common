package hooks

import (
	"bytes"
	"encoding/json"
	"github.com/sirupsen/logrus"
	"net/http"
	"time"
)

type SumoLogicHook struct {
	url    string
	host   string
	levels []logrus.Level
	tags   []string
}

type SumoLogicMessage struct {
	Host    string      `json:"host"`
	Tags    []string    `json:"tags"`
	Level   string      `json:"level"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func NewSumoLogicHook(url string, host string, level logrus.Level, tags []string) *SumoLogicHook {
	var levels []logrus.Level
	for _, l := range []logrus.Level{logrus.PanicLevel, logrus.FatalLevel, logrus.ErrorLevel, logrus.WarnLevel, logrus.InfoLevel, logrus.DebugLevel} {
		if l <= level {
			levels = append(levels, l)
		}
	}
	return &SumoLogicHook{url: url, host: host, levels: levels, tags: tags}
}

func (hook SumoLogicHook) Fire(entry *logrus.Entry) error {
	message := SumoLogicMessage{Host: hook.host, Tags: hook.tags, Level: entry.Level.String(), Message: entry.Message, Data: entry.Data}
	payload, _ := json.Marshal(message)
	req, err := http.NewRequest("POST", hook.url, bytes.NewBuffer(payload))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{Timeout: time.Duration(30 * time.Second)}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	return nil
}

func (hook SumoLogicHook) Levels() []logrus.Level {
	return hook.levels
}
