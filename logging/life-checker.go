package logging

import (
	"github.com/sirupsen/logrus"
	"runtime"
	"sync"
	"time"
)

type lifeItem struct {
	last        time.Time
	currentTick int

	timeLoop            float64
	countBeforeCritical int
}

type lifeChecker struct {
	keys  map[string]*lifeItem
	mutex *sync.Mutex
}

func (life lifeChecker) init() *lifeChecker {
	result := lifeChecker{}
	result.keys = make(map[string]*lifeItem)
	result.mutex = &sync.Mutex{}

	/**
	Scan list
	*/
	go func() {
		for key := range result.keys {
			item := result.keys[key]
			duration := time.Since(item.last)
			if duration.Seconds() > item.timeLoop {
				item.currentTick++
				logrus.WithFields(logrus.Fields{"key": key, "last": item.last, "count": item.currentTick}).Warn("LifeChecker")
			}
			if item.currentTick > item.countBeforeCritical {
				logrus.WithFields(logrus.Fields{"key": key, "last": item.last, "count": item.currentTick}).Error("LifeChecker")
			}
			runtime.Gosched()
		}
		time.Sleep(20 * time.Second)
	}()

	return &result
}

func (life *lifeChecker) IamAlive(key string, timeLoop float64, countBeforeCritical int) {
	life.mutex.Lock()
	if _, ok := life.keys[key]; !ok {
		life.keys[key] = &lifeItem{last: time.Now(), currentTick: 0, timeLoop: timeLoop, countBeforeCritical: countBeforeCritical}
	} else {
		life.keys[key].currentTick = 0
		life.keys[key].last = time.Now()
	}
	defer life.mutex.Unlock()
}

var LifeChecker = lifeChecker{}.init()
