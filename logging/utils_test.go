package logging

import (
	"github.com/sirupsen/logrus"
	"testing"
)

func TestStringToLogLevel(t *testing.T) {
	if StringToLogLevel("panic") != logrus.PanicLevel {
		t.Error("Not panic")
	}
	if StringToLogLevel("fatal") != logrus.FatalLevel {
		t.Error("Not panic")
	}
	if StringToLogLevel("error") != logrus.ErrorLevel {
		t.Error("Not error")
	}
	if StringToLogLevel("warn") != logrus.WarnLevel {
		t.Error("Not warn")
	}
	if StringToLogLevel("warn1") != logrus.WarnLevel {
		t.Error("Not warn")
	}
	if StringToLogLevel("info") != logrus.InfoLevel {
		t.Error("Not info")
	}
	if StringToLogLevel("debug") != logrus.DebugLevel {
		t.Error("Not debug")
	}

}
