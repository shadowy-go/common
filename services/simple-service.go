package services

import (
	"github.com/sirupsen/logrus"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type ServiceClearCallback func()

func InitAsService(clear ServiceClearCallback) {
	logrus.Info("InitAsService")
	// setup signal catching
	signalChannel := make(chan os.Signal, 1)
	// catch all signals since not explicitly listing
	signal.Notify(signalChannel, syscall.SIGKILL|syscall.SIGQUIT)
	// method invoked upon seeing signal
	go func() {
		data := <-signalChannel
		logrus.Info("Received signal: %s", data)
		clear()
		os.Exit(1)
	}()

	for {
		var timeout = time.Duration(2000+rand.Intn(3000)) * time.Millisecond
		time.Sleep(timeout)
	}
}
