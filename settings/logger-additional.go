package settings

type LoggerAdditional struct {
	SumoLogic *LoggerSumoLogic `yaml:"sumoLogic"`
}

func (logger LoggerAdditional) init(mode string) {
	if logger.SumoLogic != nil {
		logger.SumoLogic.init(mode)
	}
}
