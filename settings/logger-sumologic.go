package settings

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy-go/common/logging"
	"gitlab.com/shadowy-go/common/logging/hooks"
)

type LoggerSumoLogic struct {
	Url   string   `yaml:"url"`
	Host  string   `yaml:"host"`
	Tags  []string `yaml:"tags"`
	Level string   `yaml:"level"`
}

func (sumoLogic LoggerSumoLogic) init(mode string) {
	var hook = hooks.NewSumoLogicHook(sumoLogic.Url, sumoLogic.Host, logging.StringToLogLevel(sumoLogic.Level), sumoLogic.Tags)
	logrus.AddHook(hook)
}
