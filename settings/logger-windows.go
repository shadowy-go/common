// +build windows

package settings

import (
	"github.com/mattn/go-colorable"
	"github.com/sirupsen/logrus"
	"strings"
)

func (logger *Logger) special(mode string) {
	if strings.ToLower(mode) != "server" {
		logrus.SetFormatter(&logrus.TextFormatter{ForceColors: true})
		logrus.SetOutput(colorable.NewColorableStdout())
	}
}
