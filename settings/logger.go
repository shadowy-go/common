package settings

import (
	"flag"
	"github.com/lestrrat-go/file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy-go/common"
	"gitlab.com/shadowy-go/common/logging"
	"io/ioutil"
	"strings"
	"time"
)

type Logger struct {
	Filename   string            `yaml:"filename"`
	DaysStore  int               `yaml:"daysStore"`
	Level      string            `yaml:"level"`
	Additional *LoggerAdditional `yaml:"additional"`
}

func (logger *Logger) Init() {
	if logger.Level == "" {
		logger.Level = "info"
	}
	var level = logging.StringToLogLevel(logger.Level)
	logrus.SetLevel(level)
	mode := flag.Lookup("mode").Value.String()
	logger.Filename = common.GetAbsolutePath(logger.Filename)
	if strings.ToLower(mode) == "server" {
		writer, _ := rotatelogs.New(
			logger.Filename+".%Y%m%d", rotatelogs.WithLinkName(logger.Filename),
			rotatelogs.WithMaxAge(time.Duration(24*logger.DaysStore)*time.Hour),
			rotatelogs.WithRotationTime(time.Duration(24)*time.Hour))
		pathMap := lfshook.WriterMap{}
		for _, l := range []logrus.Level{logrus.PanicLevel, logrus.FatalLevel, logrus.ErrorLevel, logrus.WarnLevel, logrus.InfoLevel, logrus.DebugLevel} {
			if l <= level {
				pathMap[l] = writer
			}
		}
		logrus.AddHook(lfshook.NewHook(pathMap, &logrus.TextFormatter{}))
		logrus.SetOutput(ioutil.Discard)
	}
	logger.special(mode)
	if logger.Additional != nil {
		logger.Additional.init(mode)
	}
}
