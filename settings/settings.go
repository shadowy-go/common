package settings

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy-go/common"
	"gopkg.in/yaml.v2"
	"os"
)

/**
Interface for settings
*/
type ISettings interface {
	/**
	Initialize properties
	*/
	Init()
}

/**
Abstract type for Settings
*/
type Settings struct {
	ISettings
}

/**
Load settings from file
*/
func (cfg Settings) LoadFromFile(settings ISettings, path string) error {
	logrus.Info("Settings.LoadFromFile")
	var file, err = os.Open(common.GetAbsolutePath(path))
	defer file.Close()
	if err != nil {
		logrus.WithError(err).Error("Settings.LoadFromFile read")
		return err
	}
	var decoder = yaml.NewDecoder(file)
	err = decoder.Decode(settings)
	if err != nil {
		logrus.WithError(err).Error("Settings.LoadFromFile parse")
		return err
	}
	settings.Init()
	return nil
}
